module Vlaplace
using Gridap
using GridapEmbedded
using LinearAlgebra
using GridapDistributed
using GridapGmsh: gmsh
using GridapGmsh
using SuiteSparse
#using GridapPETSc
 # Define geometry
 function blf(Pmat, n_Γd, nex2, dΓd, dΩ)
  e = 1
  h=0.3
  η = 1.0/h^2
  ρu = 1.0/h
  println("setup blf")
  #E(u) = Pmat2(Pmat((∇(u)+transpose(∇(u)))))
  E(u) = Pmat⋅ symmetric_gradient(u)⋅Pmat
  a1(u,v)= ∫( E(u)⊙ E(v) )*dΓd 
  #a2(u,v) = ∫( e*((Pmat(u))⋅(Pmat(v))))*dΓd
  a2(u,v) = ∫(e*(Pmat ⋅ u)⋅(Pmat ⋅v))*dΓd
  a3(u,v) = ∫(η*((u ⋅ n_Γd)⋅(v⋅n_Γd)))*dΓd
#  a4(u,v) =  ∫(ρu*((∇(u)⋅n_Γd)⋅(∇(v)⋅n_Γd)))*dΩ
  #a4(u,v) =  ∫(ρu*((∇(u))⊙(∇(v))))*dΩ
  a4(u,v)=∫(ρu*(∇(u)⋅ nex2)⋅(∇(v)⋅nex2))*dΩ
  a(u,v) =(a1(u,v)+a2(u,v)+a3(u,v)+a4(u,v))
      
 end
 function lf(Id)
  println("setup lf")
  e1 = VectorValue{3,Float64}(1,0,0)
  e2 = VectorValue{3,Float64}(0,1,0)
  e3 = VectorValue{3,Float64}(0,0,1)
 #Ps = Pmats(n_Γd)
nex(x) =1/norm(x)^2 *TensorValue{3,3,Float64}(x[1]*x[1],x[1]*x[2],x[1]*x[3],x[2]*x[1],x[2]^2,x[2]*x[3],x[3]*x[1],x[3]*x[2],x[3]^2)
Ps(x) = Id(x)-nex(x)
#  uex(x) = Ps(x) ⋅ VectorValue{3,Float64}(-x[3]^2,x[1],x[2])
uex(x)=Ps(x) ⋅  VectorValue{3,Float64}(-x[3]^2,x[1],x[2])
#  ∇uex(x) = ∇(uex(x))
#  ∇(::typeof(uex))=∇uex
 A(x) = (Ps(x) ⋅ symmetric_gradient(uex)(x)) ⋅ Ps(x) 
#  A1(x) = A(x)⋅ e1(x)
 #divgamma(x) = VectorValue{3,Float64}(tr(Ps(x) ⋅ ∇(A1)(x)),tr(Ps(x) ⋅ ∇(A⋅ e2)(x)),tr(Ps(x) ⋅ ∇(A⋅ e3)(x))) 
A1(x) = A(x)⋅ e1
 divgamma(x) = A1(x)
 rhs(x) = divgamma(x) + uex(x)
#  rhs(x)=uex(x)
 end
function domeshing()
    order = 2
  gmsh.initialize()
  gmsh.clear()
  box = gmsh.model.occ.addBox(
      -2.,-2.,-2.,4.,4.,4.)
   gmsh.model.occ.synchronize()
   gmsh.model.mesh.setSize(gmsh.model.getEntities(0),0.5)
  gmsh.model.mesh.generate(3)
gmsh.model.mesh.refine()
  gmsh.write("model.msh")
#  bgmodel = gmsh.model.getEntities(0)
  gmsh.finalize()
     n = 10
   # Background model
   partition = (n,n,n)
   pmin = 2*Point(-1,-1,-1)
   pmax = 2*Point(1,1,1)
 #  bgmodel = simplexify(CartesianDiscreteModel(pmin,pmax,partition))
   bgmodel =GmshDiscreteModel("model.msh")
return bgmodel
end
  # Select geometry
  function solvelaplace()
    bgmodel = domeshing()
  R = 0.5
  ϕ(x) = norm(x)-1
  # Forcing data
  ud = 1
  f = 10
  println("doing cutter")
lscutter = GridapEmbedded.LevelSetCutters.sphere(1, x0=Point(0.0,0.0,0.0), name="sphere")
  # Cut the background model
  cutgeo = cut(bgmodel, lscutter)

  # Setup integration meshes
  Ω = Triangulation(cutgeo,PHYSICAL)
  Γd = EmbeddedBoundary(cutgeo)

  # Setup normal vectors
  n_Γd = get_normal_vector(Γd)
#  reffe2 = ReferenceFE(lagrangian, VectorValue{3,Float64}, order)
#  fe = FESpace(Ω, reffe2)
#  n = interpolate_everywhere(n_Γd,fe)
  writevtk(Ω,"trian_O")
#  writevtk(Γd,"trian_Gd",cellfields=["normal"=>n_Γd])
  #writevtk(Γg,"trian_Gg",cellfields=["normal"=>n_Γg])
  writevtk(Triangulation(bgmodel),"bgtrian")

  # Setup Lebesgue measures
order=2
  degree = 2*order
  dΩ = Measure(Ω,degree)
  dΓd = Measure(Γd,degree)
  reffe = ReferenceFE(lagrangian, VectorValue{3, Float64}, order)
  println("temp?")
  function temp(x)
    return TensorValue{3,3, Float64}(1,0,0,0,1,0,0,0,1)
  end
  println("id?")
  Id = temp
  # Setup FESpace
  Ω_act = Triangulation(cutgeo,ACTIVE)
  writevtk(Ω_act, "trian_O_act")
  #normalspace = FESpace(Γd,reffe,conformity=:H1)
  
#  ntilde = interpolate_everywhere(n_Γd, normalspace) 
  println("setup TnT")
  V = TestFESpace(Ω_act,ReferenceFE(lagrangian,VectorValue{3,Float64},order, space=:P),conformity=:H1)
  U = TrialFESpace(V)

  # Weak form
 #   nex = ∇(ϕ)
 nex2(x) = VectorValue{3,Float64}(x[1]/norm(x),x[2]/norm(x),x[3]/norm(x))
  h = 0.3
#  Pmats(n) = (x)->identity(x)-(n⊗ n) ⋅ x
#  Pmats2(n) = (x)-> identity(x)-x⋅ (n⊗n)
  Pmats(n) = Id-(n⊗ n)
  Pmat = Pmats(n_Γd)
 # Pmat2 = Pmats2(n_Γd)
#  Pmat = (x)->x
#  Pmat2 = (x)->x

  # A = Gridap.Algebra.sparsecsr(a)

  #l(v) =∫(Pmat⋅(VectorValue{3,Float64}(1,2,3))⋅ v)*dΓd
  rhs = lf(Id)
  a = blf(Pmat,n_Γd,nex2, dΓd,dΩ)
  l(v)=∫(rhs ⋅ v)*dΓd
#    ∫( v*f ) * dΩ +
#    ∫( (γd/h)*v*ud - (n_Γd⋅∇(v))*ud ) * dΓd

  # FE problem
  println("setup FEop")
    @time op = AffineFEOperator(a,l,U,V)
    println("done setting up")
   A3=get_matrix(op)
   
   f=get_vector(op)
   print("factorize")
   @time qra=factorize(A3)
   uh = qra\f
#   uh=A\f
   println(length(f))

    gh = FEFunction(V,uh,get_dirichlet_dof_values(V))
#   @time AffineFEOperator(a,l,U,V)

  println("solve")
 # println(sqrt(length(A)))
  #ls = LinearFESolver()#PETScLinearSolver())
#  uh = solve(op)#, ls)

  # Postprocess
 # if outputfile !== nothing
    writevtk(Ω,"vectorlaplace_julia",order=2,cellfields=["uh"=>gh])
    

end

end