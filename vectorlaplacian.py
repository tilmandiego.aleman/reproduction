from netgen.csg import CSGeometry, OrthoBrick, Pnt
from ngsolve import *
from ngsolve.internal import *
from xfem import *
from xfem.lsetcurv import *
from math import pi
import pickle
import datetime
with TaskManager():
    # -------------------------------- PARAMETERS ---------------------------------
    # Mesh diameter
    maxh = 0.6
    geom ="decocube"
    # Polynomial order of FE space
    order = 2

    # Problem parameters
    reac_cf = 1
    diff_cf = 1
    c=1.2
    # Geometry
    cube = CSGeometry()
    if geom == "circle":
        phi = Norm(CoefficientFunction((x, y, z/c))) - 1

        cube.Add(OrthoBrick(Pnt(-2, -2, -2), Pnt(2, 2, 2)))
        mesh = Mesh(cube.GenerateMesh(maxh=maxh, quad_dominated=False))
    elif geom=="decocube":
        phi=(x**2+y**2-4)**2+(y**2-1)**2+(y**2+z**2-4)**2+(x**2-1)**2+(x**2+z**2-4)**2+(z**2-1)**2-13
        cube.Add(OrthoBrick(Pnt(-3, -3, -3), Pnt(3, 3, 3)))
        mesh = Mesh(cube.GenerateMesh(maxh=maxh, quad_dominated=False))
    n_cut_ref = 2
    print("refining..")
    for i in range(n_cut_ref):
        lsetp1 = GridFunction(H1(mesh, order=2))
        InterpolateToP1(phi, lsetp1)
        RefineAtLevelSet(lsetp1)
        mesh.Refine()
    lsetmeshadap = LevelSetMeshAdaptation(mesh, order=order)
    deformation = lsetmeshadap.CalcDeformation(phi)
#    deformation = None
    lset_approx = lsetmeshadap.lset_p1
#    lsetp1 = GridFunction(H1(mesh, order=1))
#    InterpolateToP1(phi, lsetp1)

#    lset_approx = lsetp1
    mesh.SetDeformation(deformation)
    # Class to compute the mesh transformation needed for higher order accuracy
    #  * order: order of the mesh deformation function
    #  * threshold: barrier for maximum deformation (to ensure shape regularity)

    # Background FESpace
    Vh = VectorH1(mesh, order=order, dirichlet=[])

    ci = CutInfo(mesh, lset_approx)
    ba_IF = ci.GetElementsOfType(IF)
    VhG = Restrict(Vh, ba_IF)

    gfu = GridFunction(VhG)

    # Coefficients / parameters:


    # Tangential projection
    n = Normalize(grad(lset_approx))

    h = specialcf.mesh_size

    eta = 100.0 / (h * h)
    rho = 1.0 / h
    def coef_grad(u):
        dirs = {1: [x], 2: [x, y], 3: [x, y, z]}
        if u.dim == 1:
            return CF(tuple([u.Diff(r) for r in dirs[mesh.dim]]))
        else:
            return CF(tuple([u[i].Diff(r) for i in range(u.dim) for r in dirs[mesh.dim]]), dims=(u.dim, mesh.dim))


    def grad(u):
        if type(u) in [ProxyFunction, GridFunction]:
            return ngsolve.grad(u)
        else:
            return coef_grad(u)

    def Pmats(n):
        return Id(3) - OuterProduct(n, n)


    Ps = Pmats(Normalize(grad(phi)))
    # define solution and right-hand side
    functions = {
        "extvsol1": ((-y - z) * x + y * y + z * z),
        "extvsol2": ((-x - z) * y + x * x + z * z),
        "extvsol3": ((-x - y) * z + x * x + y * y),
        "rhs1": -((y + z) * x - y * y - z * z) * (x * x + y * y + z * z + 1) / (x * x + y * y + z * z),
        "rhs2": ((-x - z) * y + x * x + z * z) * (x * x + y * y + z * z + 1) / (x * x + y * y + z * z),
        "rhs3": ((-x - y) * z + x * x + y * y) * (x * x + y * y + z * z + 1) / (x * x + y * y + z * z),
    }
    if geom=="circle":
        uSol = Ps*CoefficientFunction((functions["extvsol1"], functions["extvsol2"], functions["extvsol3"]))
        rhs = CoefficientFunction((functions["rhs1"], functions["rhs2"], functions["rhs3"]))
    elif geom=="decocube":
        uSol = Ps*CoefficientFunction((-(z ** 2), y, x))
        uSol=uSol.Compile()

    u, v = VhG.TnT()

    # Measure on surface
    ds = dCut(lset_approx, IF, definedonelements=ba_IF, deformation=deformation)
    # Measure on the bulk around the surface
    dx = dx(definedonelements=ba_IF, deformation=deformation)




    def eps(u):
        return Ps * Sym(grad(u)) * Ps


    def divG(u):
        if u.dim == 3:
            return Trace(grad(u) * Ps)
        if u.dim == 9:
            N = 3
            divGi = [divG(u[i, :]) for i in range(N)]
            return CF(tuple([divGi[i] for i in range(N)]))

    print("rhs..")
    begin_time = datetime.datetime.now()
    rhstemp = -Ps*divG(eps(uSol))+uSol
    print("compile rhs2..")
    rhs2 = rhstemp.Compile()
    print("compile time:")
    print(datetime.datetime.now() - begin_time)
    rhs = rhstemp
#    pickle.dump(rhs, open("rhs_"+geom+".p","wb"))
    Pmat = Id(3) - OuterProduct(n, n)
    # bilinear forms:
    a = BilinearForm(VhG, symmetric=True)
    a += (InnerProduct(Pmat * Sym(grad(u)) * Pmat, Pmat * Sym(grad(v)) * Pmat)) * ds
    a += InnerProduct(u,  v) * ds
    a += (eta * ((u * n) * (v * n))) * ds
    a += rho * ((grad(u) * n) * (grad(v) * n)) * dx
    print("assemble a..")
    a.Assemble()

    f = LinearForm(VhG)
    f += rhs * v * ds
    g = LinearForm(VhG)
    g += rhs2*v*ds
    print("assemble f..")
    begin_time = datetime.datetime.now()
    print("Assemble time without compile:")

    f.Assemble()
    print(datetime.datetime.now() - begin_time)
    begin_time = datetime.datetime.now()
    print("Assemble time with compile:")

    f.Assemble()
    print(datetime.datetime.now() - begin_time)
    gfu.vec[:]=0.0
    print("invert..")
    gfu.vec.data = a.mat.Inverse(VhG.FreeDofs(), inverse="pardiso") * f.vec

    print("l2error : ", sqrt(Integrate((gfu - Ps*uSol)**2 * ds, mesh=mesh)))
#    Draw(deformation, mesh, "deformation")
    Draw(gfu, mesh, "u")
    uerr = gfu-uSol
    if (len(sys.argv) < 2 or not (sys.argv[1] == "testmode")) and False:
        print("write vtk file..")
        vtk = VTKOutput(ma=mesh,
                        coefs=[lset_approx, gfu, uSol, uerr],
                        names=["P1-levelset", "uh", "uex", "uerr"],
                        filename="vectorlaplacepenalty3d", subdivision=2)
        vtk.Do()


