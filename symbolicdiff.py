from sage.all import *
#from sage.manifolds.all import *
#E=EuclideanSpace(3)
def Pmats(n):
    return identity_matrix(3) - (n.tensor_product(n))

x,y,z = var('x y z')
def eps(u, Ps):
    return Ps * (jacobian(u, (x,y,z))+jacobian(u, (x,y,z)).T) * Ps
def divvec(u, Ps):
    return (Ps*jacobian(u, (x,y,z))*Ps).trace()
def divG(u, Ps):
    return [divvec(eps(u,Ps)[k,:], Ps) for k in range(3)]

phi=sqrt(x**2+y**2+z**2)-1
u = [x,y,z]
n=phi.gradient()
Pmat = Pmats(n)
ep = divG(u, Pmat)
result=str(simplify(ep)).replace("x", "pnt[0]").replace("^", "**").replace("y", "pnt[1]").replace("z","pnt[2]")

with open("mycoeff.cpp", "w") as file:
    file.write("#include <fem.hpp>")
    file.write("""namespace ngfem \n
        {
        class MyCoefficientFunction : public CoefficientFunction
        {
        public:
            MyCoefficientFunction()
            : CoefficientFunction(/*dimension = */ 1) { ; }

            // ********************* Necessary **********************************
            virtual double Evaluate(const BaseMappedIntegrationPoint& mip) const override
            {
            // maybe do something with mip and trafo?
            auto pnt = mip.GetPoint();
            return """ + result + ";} };}")
from ngsolve.fem import CompilePythonModule
from pathlib import Path

with open("mymodule.cpp") as infile:
    data = infile.read()
m = CompilePythonModule(data, init_function_name='mymodule', add_header=False)


