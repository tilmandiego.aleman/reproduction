#!/usr/bin/python3
from killing import *

if __name__=="__main__":
    parser = argparse.ArgumentParser(description="See what happens if we only have almost-killing VF")
    parser.add_argument("--refinements", dest="n_cut_ref", type=int, required=False, default=2)
    parser.add_argument("--write-vtk", dest="vtkout", action='store_true')
    parser.add_argument("--order", dest="order", type=int, required=False, default=2)
    parser.add_argument("--geometry", dest="geom", type=str, required=False, default="ellipsoid",
                        help="Geometry to be considered. Only ellipsoid with parameter c allowed right now")
    parser.add_argument("-c", dest="c", type=float, required=False, default=1,
                        help="Parameter for the Level Set x^2+y^2+(z/c)^2-1=0")
    parser.add_argument("-o", dest="fname", type=str, required=False, default="kvfs",
                        help="Output file name for vtk")
    parser.add_argument("--plot", action='store_true')
    parser.add_argument("--saveplot", action='store_true')
    parser.add_argument("--alpha", dest="alpha", type=float, required=False, default=3)
    parser.add_argument("--radius", dest="radius", type=float, required=False, default=1)
    parser.add_argument("--log", dest="loglevel", type=str, required=False, default="NOTSET")
    parser.add_argument("-l", dest="sca", type=float, required=False, default=2)
    args = parser.parse_args()
    maxh = 0.6*args.radius
    # Geometry
    geom = args.geom
    # Polynomial order of FE space
    order = args.order  # =args["order"]
    vtkout = args.vtkout
    n_cut_ref = args.n_cut_ref
    if order == 2:
        hs = 10 ** 6
    else:
        hs = 10 ** 8

    # Ellipsoid parameter
    c =args.c
    maxk=0
    # Geometry
    # SetHeapSize(10**9)
    # SetNumThreads(48)


    cube = CSGeometry()
    phi = (Norm(CoefficientFunction((x, y/1.5, z / c))) - args.radius).Compile()#realcompile=True)

    cube.Add(OrthoBrick(Pnt(-2, -2, -2), Pnt(2, 2, 2)))
    mesh = Mesh(cube.GenerateMesh(maxh=maxh, quad_dominated=False))

    # When interpolating, getting good results requires more refinements with complex geometries, e.g. the decocube

    # Class to compute the mesh transformation needed for higher order accuracy
    #  * order: order of the mesh deformation function
    #  * threshold: barrier for maximum deformation (to ensure shape regularity)

    lsetmeshadap = LevelSetMeshAdaptation(mesh, order=order, threshold=1000, discontinuous_qn=True, heapsize=hs)
    deformation = lsetmeshadap.CalcDeformation(phi)
    lset_approx = lsetmeshadap.lset_p1

    mesh.SetDeformation(deformation)

    # Background FESpaces

    # Helper Functions for tangential projection and gradients of the exact solution

    # Coefficients / parameters:

    # Exact tangential projection
    Ps = Pmats(Normalize(grad(phi, mesh)))  # .Compile(realcompile=True)

    # define solution and right-hand side


    uSol = (Ps * CF((-z ** 2, x, y)))  # .Compile(realcompile=True)
    kvfs = [CoefficientFunction((y, -x, 0)), CF((z, 0, -x)), CF((0, z, -y))]

    weingex = grad(Normalize(grad(phi, mesh)), mesh).Compile()#realcompile=True)
    l2errors = []
    l2errkf = []
    vlams = []
    symmetries = 0
    condense=True
    
    for i in range(n_cut_ref + 1):
        print("refinement is: ", i)
        if i==0:
#            a,b,pre, Vh, VhG = getevproblem(mesh, phi, lset_approx, deformation, order)
            pass
        if i != 0:
            lset_approx, deformation = refineMesh(mesh, phi, order)
        
    vlam, ev, VhG, a,pre, ci,start = computeEV(phi,mesh,lset_approx, deformation, order, condense=condense)
    print("time: ", time.time()-start)
    print("condense: ", condense)
    print("eigenvalues:", vlam)
        
    print("starting solve..")
    print("symmetries: ", symmetries)
    with TaskManager():
        l2err, l2errnokf, uerr, uh, uSolnokf = getKilling(mesh, lset_approx, deformation, vlam, ev, VhG, a, pre, ci, step=i, alpha=args.alpha, maxsym = symmetries, maxsymex = maxk, condense=condense, disturbed=False, Ps=Ps, order=order, phi=phi)
    l2errors.append(l2err)
    l2errkf.append(l2errnokf)
    print(l2errors)
    print("error with no killing field: ")
    print(l2errkf)