#!/usr/bin/python3
"""
In this example we solve a surface stokes problem with a
consistent penalty method following [1]

Used features:
--------------
* Higher order geometry approximation, cf. jupyter tutorial `lsetint`

* Restricted finite element space to condense the system to active dofs,
  cf. jupyter tutorial `basics`

* Visualization: The visualization of the solution is most convenient
  with paraview and the generated vtk file.

Literature:
-----------
[1] T. Jankuhn, A. Reusken, Higher order Trace Finite Element Methods for the Surface Stokes Equation.
    arXiv preprint arXiv:1909.08327, 2019.

"""

# ------------------------------ LOAD LIBRARIES -------------------------------

import argparse


from helperfunctions import *


# -------------------------------- PARAMETERS ---------------------------------

def solveStokes(mesh, lset_approx, deformation, step, alpha=3):
    uSol = (Ps * CF((-z ** 2, x, y)))
    Vh = VectorH1(mesh, order=order, dirichlet=[])
    Qh = H1(mesh, order=order - 1, dirichlet=[])

    ci = CutInfo(mesh, lset_approx)
    ba_IF = ci.GetElementsOfType(IF)
    VhG = Restrict(Vh, ba_IF)
    L2G = Restrict(Qh, ba_IF)

    X = VhG * L2G
    gfu = GridFunction(X)

    n = Normalize(grad(lset_approx))
    ntilde = GridFunction(VhG)
    ntilde.Set(Normalize(grad(phi, mesh)), definedonelements=ba_IF)
    #        n=ntilde
    h = specialcf.mesh_size
    #elvol = Integrate(levelset_domain={"levelset":phi, "domain_type":IF},cf=CoefficientFunction(1),mesh=mesh,element_wise=True)
    #cuthmax = max([(2*vol)**(1/2) for vol in elvol])
    #        epsilon =(cuthmax)**(alpha)
    epsilon = h ** alpha
    print("Epsilon:")
    print(epsilon)
    #        epsilon=1
    eta = 1 / (h) ** 2
    rhou = 1.0 / h
    rhop = h
    rhob = h
    # Measure on surface
    ds = dCut(lset_approx, IF, definedonelements=ba_IF, deformation=deformation)
    # Measure on the bulk around the surface
    dX = dx(definedonelements=ba_IF, deformation=deformation)

    u, p = X.TrialFunction()
    v, q = X.TestFunction()

    # Weingarten mappings

    ninter = GridFunction(VhG)
    ninter.Set(n, definedonelements=ba_IF)
    weing = grad(ninter)

    #        rhs1ex = rhsfromsol(uSol, pSol, interpol=False)
    L21 = L2(mesh, order=order + 1, dim=9)
    L22 = L2(mesh, order=order + 1, dim=3)
    lpmat = Restrict(L21, ba_IF)
    lpvec = Restrict(L22, ba_IF)
    if step > 4:
        comp = True
    else:
        comp = False
    print("came here ")
    rhs1 = rhsfromsol(uSol, pSol, phi, Ps, mesh, ba_IF, weingex, interpol, lpmat, lpvec).Compile(realcompile=comp)
    rhs2 = Trace(Ps * grad(uSol, mesh)).Compile(realcompile=comp)

    Pmat = Pmats(n)

    def E(u):
        return (Pmat * Sym(grad(u)) * Pmat) - (u * n) * weing

    a = BilinearForm(X, symmetric=True)
    a += InnerProduct(E(u), E(v)).Compile(realcompile=comp) * ds
    a += epsilon * (Pmat * u) * (Pmat * v) * ds
    a += (eta * ((u * ntilde) * (v * ntilde))) * ds
    a += rhou * ((grad(u) * n) * (grad(v) * n)) * dX
    a += InnerProduct(u, Pmat * grad(q)) * ds
    a += InnerProduct(v, Pmat * grad(p)) * ds
    a += -rhop * InnerProduct(n * grad(p), n * grad(q)) * dX

    u2, v2 = VhG.TnT()
    a2 = BilinearForm(VhG, symmetric=True)

    a2 += InnerProduct(E(u2), E(v2)) * ds
    a2 += (Pmat * u2) * (Pmat * v2) * ds
    a2 += rhou * ((grad(u2) * n) * (grad(v2) * n)) * dX
    a2 += (eta * ((u2 * ntilde) * (v2 * ntilde))) * ds
    pre = Preconditioner(a2, "direct")
    a2.Assemble()

    b = BilinearForm(VhG, symmetric=True)
    b += InnerProduct(Pmat * u2, Pmat * v2) * ds
    b += rhob * (grad(u2) * n) * (grad(v2) * n) * dX
    b += ((u2 * n) * (v2 * n)) * ds
    b.Assemble()

    vlam, evecs = solvers.PINVIT(a2.mat, b.mat, pre, 3, GramSchmidt=True)
    evecinter = []

    for k in range(3):
        #            evn = sqrt(Integrate(levelset_domain={"levelset": phi, "domain_type": IF}, cf=(evecs3.MDComponent(k)) ** 2,
        #                                 mesh=mesh))
        #            evecs.vecs[k].data = evecs3.vecs[k].data
        evecinter.append(GridFunction(VhG))
        evecinter[k].vec.data = evecs[k].data

    f = LinearForm(X)
    f += rhs1 * v * ds
    f += -rhs2 * q * ds

    f.Assemble()
    a.Assemble()

    egfu = GridFunction(VhG)
    gfu.vec.data = a.mat.Inverse(freedofs=X.FreeDofs(), inverse="pardiso") * f.vec

    print("removing killing VFs")

    kf = GridFunction(VhG)
    print(vlam)


    kf2 = GridFunction(VhG)
    for k in range(maxk):
        #                kf2.Set(kvfs[k],definedonelements=ba_IF)
        #                kfnorm = Integrate(levelset_domain={"levelset":phi, "domain_type":IF}, cf=InnerProduct(kf2,kf2), mesh=mesh, order=5)
        #                integral = -Integrate(levelset_domain={"levelset":phi, "domain_type":IF}, cf=InnerProduct(gfu2.components[0],kf2), mesh=mesh, order=5)/kfnorm

        kfexnorm = Integrate(levelset_domain={"levelset": phi, "domain_type": IF},
                             cf=InnerProduct(kvfs[k], kvfs[k]), mesh=mesh, order=5)
        uSol = uSol - Integrate(levelset_domain={"levelset": phi, "domain_type": IF},
                                cf=InnerProduct(uSol, kvfs[k]), mesh=mesh, order=5) / kfexnorm * kvfs[k]

    #                gfu2.components[0].vec.data += integral*kf2.vec
    kf.Set(uSol, definedonelements=ba_IF)
    kf2.vec.data = gfu.components[0].vec.data - kf.vec.data
    for k in range(0):
        kf.Set(kvfs[k], definedonelements=ba_IF)
        integral = -Integrate(levelset_domain={"levelset": phi, "domain_type": IF},
                              cf=InnerProduct(kf2, kvfs[k]), mesh=mesh, order=5) / Integrate(
            levelset_domain={"levelset": phi, "domain_type": IF},
            cf=InnerProduct(kvfs[k], kvfs[k]), mesh=mesh, order=5)
        kf2.vec.data += integral * kf.vec
    #                gfu.components[0].vec.data +=-Integrate(levelset_domain={"levelset": phi, "domain_type": IF},
    #                                cf=InnerProduct(gfu.components[0], kvfs[k]), mesh=mesh, order=5) / kfexnorm * kf.vec
    # gfu2.vec.data = a.mat.Inverse(freedofs=X.FreeDofs(), inverse="pardiso")*f2.vec

    removekf(VhG, gfu, evecinter, vlam, phi, mesh, maxk=maxk)
    l2err = sqrt(
        Integrate(levelset_domain={"levelset": phi, "domain_type": IF}, cf=(gfu.components[0] - uSol) ** 2, mesh=mesh))
    l2errnokf = sqrt(
        Integrate(levelset_domain={"levelset": phi, "domain_type": IF}, cf=InnerProduct(kf2, kf2), mesh=mesh))
    uerr = gfu.components[0] - uSol

    print("l2error : ", l2err)
    print("error without killing error: ", l2errnokf)
    return l2err, l2errnokf, kf2, gfu, uSol


if __name__ == "__main__":
    # Interpolation for the rate-of-strain tensor when computing the r.h.s.
    # Introduces an interpolation error, but significantly improves performance
    parser = argparse.ArgumentParser(description="Solve the Surface Stokes problem")
    parser.add_argument("--refinements", dest="n_cut_ref", type=int, required=False, default=2)
    parser.add_argument("--write-vtk", dest="vtkout", action='store_true')
    parser.add_argument("--order", dest="order", type=int, required=False, default=2)
    parser.add_argument("--geometry", dest="geom", type=str, required=False, default="ellipsoid",
                        help="Geometry to be considered. Only ellipsoid with parameter c allowed right now")
    parser.add_argument("-c", dest="c", type=float, required=False, default=1,
                        help="Parameter for the Level Set x^2+y^2+(z/c)^2-1=0")
    parser.add_argument("-o", dest="fname", type=str, required=False, default="stokestracefem3d",
                        help="Output file name for vtk")
    parser.add_argument("--plot", action='store_true')
    parser.add_argument("--saveplot", action='store_true')
    parser.add_argument("--alpha", dest="alpha", type=float, required=False, default=3)
    
    args = parser.parse_args()
    interpol = True
    # Mesh diameter
    maxh = 0.6
    # Geometry
    geom = args.geom
    # Polynomial order of FE space
    order = args.order  # =args["order"]
    vtkout = args.vtkout
    n_cut_ref = args.n_cut_ref
    if order == 2:
        hs = 10 ** 6
    else:
        hs = 10 ** 8

    # Ellipsoid parameter
    c = args.c
    if c == 1 and geom=="ellipsoid":
        maxk = 3
    elif c>1 and geom=="ellipsoid":
        maxk = 1
    elif geom=="arrow":
        maxk=1
    # Geometry
    # SetHeapSize(10**9)
    # SetNumThreads(48)

    cube = CSGeometry()
    if geom == "ellipsoid":
        phi = Norm(CoefficientFunction((x, y, z / c))) - 1

        cube.Add(OrthoBrick(Pnt(-2, -2, -2), Pnt(2, 2, 2)))
        mesh = Mesh(cube.GenerateMesh(maxh=maxh, quad_dominated=False))
    elif geom=="arrow":
        phi =x**2+y**2+(z/1.5)**2+c*(x*y)**2 - 1

        cube.Add(OrthoBrick(Pnt(-2, -2, -2), Pnt(2, 2, 2)))
        mesh = Mesh(cube.GenerateMesh(maxh=maxh, quad_dominated=False))
    elif geom == "decocube":
        phi = (x ** 2 + y ** 2 - 4) ** 2 + (y ** 2 - 1) ** 2 + (y ** 2 + z ** 2 - 4) ** 2 + (x ** 2 - 1) ** 2 + (
                x ** 2 + z ** 2 - 4) ** 2 + (z ** 2 - 1) ** 2 - 13
        cube.Add(OrthoBrick(Pnt(-3, -3, -3), Pnt(3, 3, 3)))
        mesh = Mesh(cube.GenerateMesh(maxh=maxh, quad_dominated=False))

    # Preliminary refinements. As assembling the r.h.s. is very expensive without interpolation, keep it small in that case.
    # When interpolating, getting good results requires more refinements with complex geometries, e.g. the decocube

    # Class to compute the mesh transformation needed for higher order accuracy
    #  * order: order of the mesh deformation function
    #  * threshold: barrier for maximum deformation (to ensure shape regularity)

    lsetmeshadap = LevelSetMeshAdaptation(mesh, order=order, threshold=1000, discontinuous_qn=True, heapsize=hs)
    deformation = lsetmeshadap.CalcDeformation(phi)
    lset_approx = lsetmeshadap.lset_p1

    mesh.SetDeformation(deformation)

    # Background FESpaces

    # Helper Functions for tangential projection and gradients of the exact solution

    # Coefficients / parameters:

    # Exact tangential projection
    Ps = Pmats(Normalize(grad(phi, mesh)))  # .Compile(realcompile=True)

    # define solution and right-hand side

    if geom == "ellipsoid" or geom=="arrow":
        """
        functions = {
            "extvsol1": ((-y - z) * x + y * y + z * z),
            "extvsol2": ((-x - z) * y + x * x + z * z),
            "extvsol3": ((-x - y) * z + x * x + y * y),
            "rhs1": -((y + z) * x - y * y - z * z) * (x * x + y * y + z * z + 1) / (x * x + y * y + z * z),
            "rhs2": ((-x - z) * y + x * x + z * z) * (x * x + y * y + z * z + 1) / (x * x + y * y + z * z),
            "rhs3": ((-x - y) * z + x * x + y * y) * (x * x + y * y + z * z + 1) / (x * x + y * y + z * z),
        }
        pSol = (x * y ** 3 + z * (x ** 2 + y ** 2 + z ** 2) ** (3 / 2)) / ((x ** 2 + y ** 2 + z ** 2) ** 2)
        uSol = Ps* CoefficientFunction((functions["extvsol1"], functions["extvsol2"], functions["extvsol3"]))"""
        uSol = (Ps * CF((-z ** 2, x, y)))  # .Compile(realcompile=True)
        pSol = CF(x * y ** 3 + z)
        kvfs = [CoefficientFunction((y, -x, 0)), CF((z, 0, -x)), CF((0, z, -y))]

    elif geom == "decocube":
        uSol = Ps * CoefficientFunction((-z ** 2, y, x))
        pSol = CoefficientFunction(
            x * y ** 3 + z - 1 / Integrate({"levelset": lset_approx, "domain_type": IF}, cf=CoefficientFunction(1),
                                           mesh=mesh) * Integrate({"levelset": lset_approx, "domain_type": IF},
                                                                  cf=x * y ** 3 + z, mesh=mesh))

    weingex = grad(Normalize(grad(phi, mesh)), mesh).Compile(realcompile=True)
    l2errors = []
    l2errkf = []
    for i in range(n_cut_ref + 1):
        if i != 0:
            lset_approx, deformation = refineMesh(mesh, phi, order)

        print("starting solve..")
        with TaskManager():
            l2err, l2errnokf, uerr, uh, uSolnokf = solveStokes(mesh, lset_approx, deformation, step=i, alpha=args.alpha)
        l2errors.append(l2err)
        l2errkf.append(l2errnokf)

    print(l2errors)
    print("error with no killing field: ")
    print(l2errkf)
    if args.plot:
        if geom=="ellipsoid" and c==1:

            plot_errors(l2errors, l2errkf, "circle", args.saveplot, order=order, refs=n_cut_ref, c=c)

        else:
            plot_errors(l2errors, l2errkf, "ellipsoid", args.saveplot, order=order, refs=n_cut_ref, c=c)

    if vtkout:  # and len(sys.argv) < 2 or not (sys.argv[1] == "testmode"):
        #    input("Continue (press enter) to create a VTK-Output to stokestracefem3d.vtk")

        #    with TaskManager():

        vtk = VTKOutput(ma=mesh,
                        coefs=[lset_approx, uh.components[0], uerr, uSolnokf],
                        names=["P1-levelset", "uh", "uerr", "uex"],
                        filename=args.fname, subdivision=0, legacy=False)

        vtk.Do()
