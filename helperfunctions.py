from ngsolve import *
from xfem.lsetcurv import *
import matplotlib.pyplot as plt
from netgen.csg import CSGeometry, OrthoBrick, Pnt
import math
import scipy
import time
import logging as lg
from functools import reduce
import scipy.sparse as sp
import matplotlib.pyplot as plt
import asyncio
from asgiref.sync import async_to_sync
from ngsolve.la import EigenValues_Preconditioner
def getVertexBlock(*args):
    v=args[0][0]
    VhG=args[0][1]
    mesh=args[0][2]
    data = set(d for el in mesh.vertices[v].elements for d in VhG.GetDofNrs(el))
    return data

def refineMesh(mesh, phi, order, hs=10 ** 6):
    if order>2:
        hs=10**8
    with TaskManager():
        mesh.SetDeformation(None)
        
        
        lsetp1 = GridFunction(H1(mesh, order=order))
        print("lsetp1 interpolation..")
        InterpolateToP1(phi, lsetp1)
        print("lsetp1 refinement..")
        RefineAtLevelSet(lsetp1)

        mesh.Refine()
        print("lsetmeshadap...")
        lsetmeshadap = LevelSetMeshAdaptation(mesh, order=order, threshold=1000, discontinuous_qn=True, heapsize=hs)
        print("calc deform...")
        deformation = lsetmeshadap.CalcDeformation(phi)
        print("set deform..")
        if order>1:
            mesh.SetDeformation(deformation)
        lset_approx = lsetmeshadap.lset_p1
    return lset_approx, deformation


def rhsfromsol(uSol, pSol, phi, Ps, mesh, ba_IF, weingex, interpol=True, Lpmat=None, Lpvec=None):
    if interpol:

        tmpeps = GridFunction(Lpmat)
        tmpeps.Set(eps(uSol, Ps, mesh) - (uSol * Normalize(grad(phi, mesh))) * weingex, definedonelements=ba_IF)
        tmptotal = GridFunction(Lpvec)
        tmptotal.Set(Ps * (-divG(tmpeps, Ps, mesh) + grad(pSol, mesh)), definedonelements=ba_IF)
        return tmptotal
    else:

        return Ps * (-divG(eps(uSol, Ps).Compile())) + Ps * grad(pSol, mesh).Compile()


def coef_grad(u, mesh):
    dirs = {1: [x], 2: [x, y], 3: [x, y, z]}
    if u.dim == 1:
        return CF(tuple([u.Diff(r) for r in dirs[mesh.dim]]))
    else:
        return CF(tuple([u[i].Diff(r) for i in range(u.dim) for r in dirs[mesh.dim]]), dims=(u.dim, mesh.dim))


def grad(u, mesh=None):
    if type(u) in [ProxyFunction, GridFunction, ComponentGridFunction]:
        return ngsolve.grad(u)
    else:
        return coef_grad(u, mesh)


def Pmats(n):
    return Id(3) - OuterProduct(n, n)


# Helper Functions for rate-of-strain-tensor
def eps(u, Ps, mesh):
    return Ps * Sym(grad(u, mesh)) * Ps


def divG(u, Ps, mesh):
    if u.dim == 3:
        return Trace(grad(u, mesh) * Ps)
    if u.dim == 9:
        if u.dims[0] == 3:
            N = 3
            divGi = [divG(u[i, :]) for i in range(N)]
            return CF(tuple([divGi[i] for i in range(N)]))
        else:
            N = 3
            r = Grad(u) * Ps
            return CF(tuple([Trace(r[N * i:N * (i + 1), 0:N]) for i in range(N)]))


def l2sca(f, g, phi, mesh):
    return Integrate(levelset_domain={"levelset": phi, "domain_type": IF}, cf=InnerProduct(f, g), mesh=mesh)


def l2norm(f, phi, mesh):
    return sqrt(l2sca(f, f, phi, mesh))


def removekf(VhG, f, kvs, vlams, phi, mesh, maxk=3, tol=0, autofilter=True, inplace=False):
    print(maxk)
    for k in range(3):
        if (k < maxk and autofilter) or False:
            print(l2norm(kvs[k], phi, mesh))
            sca = l2sca(f.components[0], kvs[k], phi, mesh)
            f.components[0].vec.data += -sca * kvs[k].vec
    return
def plot_errors(l2errs, l2errskf, geom, saveplot, order=2, refs=2, c=1, sca=2):


    hvals = []
    ticks = range(1,len(l2errs)+1)
    for k in range(len(l2errs)):
        hvals.append((1 / 2 ** (order+1)) ** k)
    hvals2 = []
    
    for k in range(len(l2errs)):
        hvals2.append((1 / 2 ** order) ** k)
    
    plt.semilogy(ticks,l2errs, label=geom+" with KF removal")
    plt.semilogy(ticks, l2errskf, label=geom + " modulo discrete KF")

    plt.semilogy(ticks,hvals, label=r'$O(h^'+ str(order+1)+r')$')
    plt.semilogy(ticks,hvals2, label=r'$O(h^'+ str(order)+r')$')
    
    plt.ylabel(r'$L2$ error')
    plt.xlabel("Refinement")
    plt.title("Convergence removing KF")
    plt.legend()
    plt.xticks(ticks)
    if saveplot:
        plt.savefig("convergence_plot_"+geom+"_c"+str(c)+"_order"+str(order)+"_refs"+str(refs)+"_sca"+str(sca)+".png")
    else:
        plt.show()

def computeEV(phi, mesh, lset_approx, deformation, order=2, condense=True):
    with TaskManager():
        #condense=True
        print("starting ev comp")

        vlams = []
        if order<6:
            precond = "bddc"
        else:
            precond = "direct"
        start=time.time()
        Vh = VectorH1(mesh, order=order, dirichlet=[], low_order_space=True, wb_fulledges=True)

        ci = CutInfo(mesh, lset_approx)
        ba_IF = ci.GetElementsOfType(IF)
        VhG = Restrict(Vh, ba_IF)

        

#        VhG.SetCouplingType(IntRange(localdofs),COUPLING_TYPE.LOCAL_DOF)
        blocks = []
        freedofs = VhG.FreeDofs()
        class SymmetricGS(BaseMatrix):
            def __init__ (self, smoother):
                super(SymmetricGS, self).__init__()
                self.k=0
                self.smoother = smoother

            def Mult (self, x, y):
                y[:] = 0.0
                self.smoother.Smooth(y, x)
                self.smoother.SmoothBack(y,x)

            def Height (self):
                return self.smoother.height
            def Width (self):
                return self.smoother.height
        
        vertexdofs = BitArray(VhG.ndof)
        vertexdofs[:] = False
        print(len(vertexdofs))
        print(VhG.ndof)
        print("doing stuff for precond")
        if False:
            for v in mesh.vertices:

            
                



                                
                
    #            vdofs = set(d for d in VhG.GetDofNrs(el) if freedofs[d])
                for d in VhG.GetDofNrs(v):

                    if d>=0:
                        vertexdofs[d] = True
            
    #            blocks.append(vdofs)
            

        #blocks = [set([d  for el in mesh[v].elements for d in VhG.GetDofNrs(el)]) for v in mesh.vertices]
        import multiprocessing as mp
        from itertools import starmap
        #pool = mp.Pool(16)
        #manager = mp.Manager()
        #lock = manager.Lock()
        #blocks = manager.list()
        #blocks = [[]]*len(mesh.vertices)
        #for k in range(len(mesh.vertices)):
        #    pool.apply_async(getVertexBlock, (k, VhG, mesh, shared_list))
        it = [(k, VhG, mesh) for k in range(len(mesh.vertices))]

        blocks =list(map(getVertexBlock, it))
        #pool.close()
        
        #blocks = [el.dofs for el in VhG.Elements()]
        """
#        blocks = [reduce(lambda x,y: x or y,[set(d for d in VhG.GetDofNrs(el) if freedofs[d])for el in mesh[v].elements])   for v in mesh.vertices]
        """
        n = VhG.ndof
        markdof = [0]*n
        if False:
            for el in VhG.Elements():
                for d in VhG.GetDofNrs(el):
                    markdof[d]+=1
            #dofs = VhG.GetDofs()
            for k in range(VhG.ndof):
                if markdof[k]<2:
                    VhG.SetCouplingType(k, COUPLING_TYPE.LOCAL_DOF)
        
        def blockcr(FEspace):
            for v in FEspace.mesh.vectices:
                vdofs = set()
                for el in FEspace.mesh[v].elements:
                    vdofs |= set(d for d in FEspace.GetDofNrs(el)
                            if freedofs[d])
                    
#        blocks = pool.map(partial(calcstuff, mesh=mesh), range(len(mesh.vertices)))
        #vertexdofs &= VhG.FreeDofs()
        """
        """
        print("done with stuff for precond")
        n = Normalize(grad(lset_approx))
        ntilde = GridFunction(VhG)
        ntilde.Set(Normalize(grad(phi, mesh)), definedonelements=ba_IF)
        print("L2 error of ntilde: ", l2norm(ntilde-Normalize(grad(phi,mesh)),phi, mesh))
        #        n=ntilde
        h = specialcf.mesh_size
        #        epsilon=1
        eta = 1 / (h) ** 2
        rhou = 1.0 / h
        rhob = h
        # Measure on surface
        ds = dCut(lset_approx, IF, definedonelements=ba_IF, deformation=deformation)
        # Measure on the bulk around the surface
        dX = dx(definedonelements=ba_IF, deformation=deformation)

        u2 = VhG.TrialFunction()
        v2 = VhG.TestFunction()

        # Weingarten mappings

        ninter = GridFunction(VhG)
        ninter.Set(n, definedonelements=ba_IF)
        weing = grad(ninter)

        Pmat = Pmats(n)#.Compile()

        def E(u):
            return ((Pmat * Sym(grad(u)) * Pmat) - (u * n) * weing)#.Compile()
        
        a2 = BilinearForm(VhG, symmetric=True, condense=condense, store_inner=condense)
        a2 += InnerProduct(E(u2), E(v2)).Compile() * ds
#        a2 += (Pmat * u2) * (Pmat * v2) * ds
        a2 += rhou * ((grad(u2) * n) * (grad(v2) * n)).Compile() * dX
        a2 += (eta * ((u2 * ntilde) * (v2 * ntilde))).Compile() * ds
        coarsepre = Preconditioner(a2, "h1amg", inverse="pardiso") #+ Preconditioner(a2, "bddc", coarsetype="h1amg", inverse="pardiso")
        #pre1 = Preconditioner(a2, precond, inverse="pardiso", coarsetype="h1amg")
        #pre1 = Preconditioner(a2, "bddc", inverse="pardiso")
      


#        pre = Projector(VhG.FreeDofs(), True)
#        
        
        b = BilinearForm(VhG, symmetric=True)
        b += InnerProduct(Pmat * u2, Pmat * v2).Compile() * ds
        b += (rhob * (grad(u2) * n) * (grad(v2) * n)).Compile() * dX
        b += ((u2 * n) * (v2 * n)).Compile() * ds
        b.Assemble()
        start = time.time()
        a2.Assemble()
        #coarsepre = Preconditioner(a2, "hypre")
        #vblocks = [VhG.GetDofNrs(vertex) for el in VhG.Elements() for vertex in el.vertices ]
        #eblocks = [VhG.GetDofNrs(edge) for edge in VhG.mesh.edges]
        #fblocks = [VhG.GetDofNrs(face) for face in VhG.mesh.faces]
        #vinv = a2.mat.CreateSmoother()
        #einv = a2.mat.CreateBlockSmoother(eblocks)
        #finv = a2.mat.CreateBlockSmoother(fblocks)
        print("smoother")
        #prejac = vinv#finv@einv@vinv
        if False:
            if not condense:
                pre = pre1#+prejac
                #pre=prejac
            else:
                pre=pre1#+prejac
                #pre=prejac


        rows,cols,vals = a2.mat.COO()
        A1 = sp.csr_matrix((vals,(rows,cols)))
        plt.spy(A1)

        #plt.show()
        
        
        #coarsepre = a2.mat.Inverse(freedofs=vertexdofs, inverse="pardiso")
        #pool.join()

        prejac = SymmetricGS(a2.mat.CreateBlockSmoother(blocks, parallel=True))
        #prejac = SymmetricGS(a2.mat.CreateSmoother(VhG.FreeDofs()))
        pre=prejac+coarsepre
        #pre=pre1
        lams = EigenValues_Preconditioner(a2.mat, pre)#+coarsepre)
        with open("Condition.txt", "a") as file:
            file.write("Preconditioned EVs: "+ str(lams[-1]/lams[0])+"\n")

        #pre = pre + coarsepre
#        pre=a2.mat.Inverse(freedofs=VhG.FreeDofs(True), inverse="pardiso") + coarsepre
        print("assemble time: ", time.time()-start)

        if condense:
            ev, evecs = PINVIT2(a2, b.mat, pre, VhG, 4, GramSchmidt=True,maxit = 1)

        else:
            ev, evecs =solvers.PINVIT(a2.mat, b.mat, pre, 4, maxit=1)
        return ev, evecs, VhG, a2, pre, ci, start

def PINVIT2(a, matm, pre,fes, num=1, maxit=20, printrates=True, GramSchmidt=False):
    """preconditioned inverse iteration"""
   
    mata = (IdentityMatrix()-a.harmonic_extension_trans)@(a.mat + a.inner_matrix)@(IdentityMatrix()-a.harmonic_extension)
    matainv = (IdentityMatrix()+a.harmonic_extension)@(pre + a.inner_solve)@(IdentityMatrix()-a.harmonic_extension_trans)

    r = mata.CreateRowVector()
    
    uvecs = MultiVector(r, num)
    vecs = MultiVector(r, 2*num)
    # hv = MultiVector(r, 2*num)

    for v in vecs[0:num]:
        v.SetRandom()
    uvecs[:] = matainv * vecs[0:num]
    print("condensed")
    lams = Vector(num * [1])
    
    for i in range(maxit):
        vecs[0:num] = mata * uvecs - (matm * uvecs).Scale (lams)
        vecs[num:2*num] = matainv * vecs[0:num]
        vecs[0:num] = uvecs
        
        vecs.Orthogonalize(matm)

        # hv[:] = mata * vecs
        # asmall = InnerProduct (vecs, hv)
        # hv[:] = matm * vecs
        # msmall = InnerProduct (vecs, hv)
        asmall = InnerProduct (vecs, mata * vecs)
        msmall = InnerProduct (vecs, matm * vecs)
    
        ev,evec = scipy.linalg.eigh(a=asmall, b=msmall)
        lams = Vector(ev[0:num])
        if printrates:
            print (i, ":", list(lams))

        uvecs[:] = vecs * Matrix(evec[:,0:num])
    return lams, uvecs


