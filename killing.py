#!/usr/bin/python3

import argparse

from netgen.csg import CSGeometry, OrthoBrick, Pnt

from helperfunctions import *
import csv
from time import perf_counter

# -------------------------------- PARAMETERS ---------------------------------

def getKilling(mesh, lset_approx, deformation,  vlam, evecs, VhG, a,pre, ci, step,alpha=3, maxsym = 0, maxsymex = 0, condense=False, disturbed=False, Ps=None, order=2, phi=None):
    
    uSol = (Ps * CF((-z ** 2, x, y)))
    uSol2 = (Ps * CF((-z ** 2, x, y)))
    if False:
        for k in range(maxsymex):
            print("Removing ex. KF")
            kfexnorm = Integrate(levelset_domain={"levelset": phi, "domain_type": IF},
                                cf=InnerProduct(evecs,[k], evecs[k]), mesh=mesh, order=5)
            uSol = uSol - Integrate(levelset_domain={"levelset": phi, "domain_type": IF},
                                    cf=InnerProduct(uSol, evecs[k]), mesh=mesh, order=5) / kfexnorm * evecs[k]





    ba_IF = ci.GetElementsOfType(IF)


    X = VhG 

    gfu = GridFunction(X)

    n = Normalize(grad(lset_approx))
#    ntilde = GridFunction(VhG)
#    ntilde.Set(Normalize(grad(phi, mesh)), definedonelements=ba_IF)
    #        n=ntilde

#    elvol = Integrate(levelset_domain={"levelset":phi, "domain_type":IF},cf=CoefficientFunction(1),mesh=mesh,element_wise=True)
#    cuthmax = max([(2*vol)**(1/2) for vol in elvol])
    #        epsilon =(cuthmax)**(alpha)
#    print("number of elements: ")
#    print(len(elvol))
#    print("hmax:")
#    print(cuthmax)

    # Measure on surface
    ds = dCut(lset_approx, IF, definedonelements=ba_IF, deformation=deformation)
    # Measure on the bulk around the surface
    dX = dx(definedonelements=ba_IF, deformation=deformation)

    u = X.TrialFunction()
    v = X.TestFunction()

    # Weingarten mappings



    #        rhs1ex = rhsfromsol(uSol, pSol, interpol=False)

    if step > 10:
        comp = True
    else:
        comp = False



    """
    a = BilinearForm(X, symmetric=True, condense=condense)
    a += InnerProduct(E(u), E(v)).Compile(realcompile=comp) * ds
    a += epsilon * (Pmat * u) * (Pmat * v) * ds
    a += (eta * ((u * ntilde) * (v * ntilde))) * ds
    a += rhou * ((grad(u) * n) * (grad(v) * n)) * dX
    """
    weingex = grad(Normalize(grad(phi, mesh)), mesh).Compile()
    L21 = L2(mesh, order=order + 1, dim=9)
    L22 = L2(mesh, order=order + 1, dim=3)
    lpmat = Restrict(L21, ba_IF)
    lpvec = Restrict(L22, ba_IF)
    tmpeps = GridFunction(lpmat)
    tmpeps.Set(eps(uSol, Ps, mesh) - (uSol * Normalize(grad(phi, mesh))) * weingex, definedonelements=ba_IF)
    tmptotal = GridFunction(lpvec)
    tmptotal.Set(Ps * (-divG(tmpeps, Ps, mesh)), definedonelements=ba_IF)
    
    evecinter = []

    for k in range(3):
        #            evn = sqrt(Integrate(levelset_domain={"levelset": phi, "domain_type": IF}, cf=(evecs3.MDComponent(k)) ** 2,
        #                                 mesh=mesh))
        #            evecs.vecs[k].data = evecs3.vecs[k].data
        evecinter.append(GridFunction(VhG))
        evecinter[k].vec.data = evecs[k].data
    rhs1 = tmptotal
    if disturbed:
        rhs1 += 0.001*evecinter[0]
    f = LinearForm(X)
    f += rhs1 * v * ds


    f.Assemble()
#    pre=Preconditioner(a,"direct", inverse="pardiso")
#    a.Assemble()
    with open("Timings.txt", "a") as file:
        if True:
            start = perf_counter()
            
            #solvers.MinRes(mat=a.mat, rhs=f.vec,sol=gfu.vec,pre=pre,maxsteps=10000, tol=1e-7, printrates=False)
            inner = lambda f,g: Integrate(levelset_domain={"levelset": phi, "domain_type": IF},
                              cf=InnerProduct(f,g)+InnerProduct(E(f,g)), mesh=mesh)
            solvers.GMRes(A=a.mat, b=f.vec, pre=pre,x=gfu.vec, maxsteps=10000, tol=1e-7)
            file.write("Iterative: " + str(perf_counter()-start) + "\n")
        #else:
        start=perf_counter()
        solvers.BVP(a, f, gfu, inverse="pardiso", needsassembling=False)
        #a.mat.Inverse(freedofs=X.FreeDofs(), inverse="pardiso") * f.vec
        file.write("Direct: " + str(perf_counter()-start) + "\n")
    kf = GridFunction(VhG)
    print(vlam)


    kf2 = GridFunction(VhG)


    #                gfu2.components[0].vec.data += integral*kf2.vec
    kf.Set(uSol-gfu, definedonelements=ba_IF)
    kf2 = evecinter
    for k in range(maxsym):
        #kf.Set(kvfs[k], definedonelements=ba_IF)
        integral = -Integrate(levelset_domain={"levelset": phi, "domain_type": IF},
                              cf=InnerProduct(kf2[k], kf), mesh=mesh) 
   
        kf.vec.data += integral * kf2[k].vec
    #                gfu.components[0].vec.data +=-Integrate(levelset_domain={"levelset": phi, "domain_type": IF},
    #                                cf=InnerProduct(gfu.components[0], kvfs[k]), mesh=mesh, order=5) / kfexnorm * kf.vec
    # gfu2.vec.data = a.mat.Inverse(freedofs=X.FreeDofs(), inverse="pardiso")*f2.vec

    for k in range(maxsym):
        
        
        print("removed ", k)

        sca = l2sca(gfu, evecinter[k], phi, mesh)
        gfu.vec.data += -sca * evecinter[k].vec
    l2err = sqrt(
        Integrate(levelset_domain={"levelset": phi, "domain_type": IF}, cf=(gfu - uSol) ** 2, mesh=mesh))
    l2errnokf = sqrt(
        Integrate(levelset_domain={"levelset": phi, "domain_type": IF}, cf=InnerProduct(kf, kf), mesh=mesh))
    uerr = gfu - uSol

    return l2err, l2errnokf, uerr, gfu, uSol


if __name__ == "__main__":
    # Interpolation for the rate-of-strain tensor when computing the r.h.s.
    # Introduces an interpolation error, but significantly improves performance
    parser = argparse.ArgumentParser(description="Find the discrete Killing Vector Fields")
    parser.add_argument("--refinements", dest="n_cut_ref", type=int, required=False, default=2)
    parser.add_argument("--write-vtk", dest="vtkout", action='store_true')
    parser.add_argument("--order", dest="order", type=int, required=False, default=2)
    parser.add_argument("--geometry", dest="geom", type=str, required=False, default="ellipsoid",
                        help="Geometry to be considered. Only ellipsoid with parameter c allowed right now")
    parser.add_argument("-c", dest="c", type=float, required=False, default=1,
                        help="Parameter for the Level Set x^2+y^2+(z/c)^2-1=0")
    parser.add_argument("-o", dest="fname", type=str, required=False, default="kvfs",
                        help="Output file name for vtk")
    parser.add_argument("--plot", action='store_true')
    parser.add_argument("--saveplot", action='store_true')
    parser.add_argument("--alpha", dest="alpha", type=float, required=False, default=3)
    parser.add_argument("--radius", dest="radius", type=float, required=False, default=1)
    parser.add_argument("--log", dest="loglevel", type=str, required=False, default="NOTSET")
    parser.add_argument("-l", dest="sca", type=float, required=False, default=2)
    args = parser.parse_args()
    fname = "data_order"+str(args.order)+"_refs"+str(args.n_cut_ref)+"_c"+str(args.c)+"_symsca"+str(args.sca)
    with open(fname, 'w') as f:
        f.write("Order: "+str(args.order)+", ")
    with open(fname, 'a') as f:
        f.write("Refinemets: " + str(args.n_cut_ref)+", ")
        f.write("Geometry param: "+ str(args.c) + "\n")
        f.write("Scaling for symmetry: "+ str(args.sca))
    numeric_level = getattr(lg, args.loglevel.upper())
    #lg.basicConfig(level=numeric_level)
    interpol = True
    # Mesh diameter
    maxh = 0.6*args.radius
    # Geometry
    geom = args.geom
    # Polynomial order of FE space
    order = args.order  # =args["order"]
    vtkout = args.vtkout
    n_cut_ref = args.n_cut_ref
    if order == 2:
        hs = 10 ** 6
    else:
        hs = 10 ** 8

    # Ellipsoid parameter
    c = args.c
    if c == 1 and geom=="ellipsoid":
        maxk = 3
    elif c>1 and geom=="ellipsoid":
        maxk = 1
    elif geom=="arrow":
        maxk=0
    # Geometry
    # SetHeapSize(10**9)
    # SetNumThreads(48)

    cube = CSGeometry()
    if geom == "ellipsoid":
        phi = (Norm(CoefficientFunction((x, y, z / c))) - args.radius).Compile()#realcompile=True)

        cube.Add(OrthoBrick(Pnt(-2, -2, -2), Pnt(2, 2, 2)))
        mesh = Mesh(cube.GenerateMesh(maxh=maxh, quad_dominated=False))
    elif geom=="arrow":
        phi =x**2+y**2+(z/1.5)**2+c*(x*y)**2 - 1

        cube.Add(OrthoBrick(Pnt(-2, -2, -2), Pnt(2, 2, 2)))
        mesh = Mesh(cube.GenerateMesh(maxh=maxh, quad_dominated=False))
    elif geom == "decocube":
        phi = (x ** 2 + y ** 2 - 4) ** 2 + (y ** 2 - 1) ** 2 + (y ** 2 + z ** 2 - 4) ** 2 + (x ** 2 - 1) ** 2 + (
                x ** 2 + z ** 2 - 4) ** 2 + (z ** 2 - 1) ** 2 - 13
        cube.Add(OrthoBrick(Pnt(-3, -3, -3), Pnt(3, 3, 3)))
    # When interpolating, getting good results requires more refinements with complex geometries, e.g. the decocube

    # Class to compute the mesh transformation needed for higher order accuracy
    #  * order: order of the mesh deformation function
    #  * threshold: barrier for maximum deformation (to ensure shape regularity)

    lsetmeshadap = LevelSetMeshAdaptation(mesh, order=order, threshold=1000, discontinuous_qn=True, heapsize=hs)
    deformation = lsetmeshadap.CalcDeformation(phi)
    lset_approx = lsetmeshadap.lset_p1

    mesh.SetDeformation(deformation)

    # Background FESpaces

    # Helper Functions for tangential projection and gradients of the exact solution

    # Coefficients / parameters:

    # Exact tangential projection
    Ps = Pmats(Normalize(grad(phi, mesh)))  # .Compile(realcompile=True)

    # define solution and right-hand side

    if geom == "ellipsoid" or geom=="arrow":
        """
        functions = {
            "extvsol1": ((-y - z) * x + y * y + z * z),
            "extvsol2": ((-x - z) * y + x * x + z * z),
            "extvsol3": ((-x - y) * z + x * x + y * y),
            "rhs1": -((y + z) * x - y * y - z * z) * (x * x + y * y + z * z + 1) / (x * x + y * y + z * z),
            "rhs2": ((-x - z) * y + x * x + z * z) * (x * x + y * y + z * z + 1) / (x * x + y * y + z * z),
            "rhs3": ((-x - y) * z + x * x + y * y) * (x * x + y * y + z * z + 1) / (x * x + y * y + z * z),
        }
        pSol = (x * y ** 3 + z * (x ** 2 + y ** 2 + z ** 2) ** (3 / 2)) / ((x ** 2 + y ** 2 + z ** 2) ** 2)
        uSol = Ps* CoefficientFunction((functions["extvsol1"], functions["extvsol2"], functions["extvsol3"]))"""
        uSol = (Ps * CF((-z ** 2, x, y)))  # .Compile(realcompile=True)
        pSol = CF(x * y ** 3 + z)
        kvfs = [CoefficientFunction((y, -x, 0)), CF((z, 0, -x)), CF((0, z, -y))]

    elif geom == "decocube":
        uSol = Ps * CoefficientFunction((-z ** 2, y, x))
        pSol = CoefficientFunction(
            x * y ** 3 + z - 1 / Integrate({"levelset": lset_approx, "domain_type": IF}, cf=CoefficientFunction(1),
                                           mesh=mesh) * Integrate({"levelset": lset_approx, "domain_type": IF},
                                                                  cf=x * y ** 3 + z, mesh=mesh))

    weingex = grad(Normalize(grad(phi, mesh)), mesh).Compile()#realcompile=True)
    l2errors = []
    l2errkf = []
    vlams = []
    symmetries = 0
    condense=True
    fnameeig = fname + "eig.csv"
    header = ["Refinement"]
    for k in range(1,5):
        header += ["\\lambda_"+str(k)+"(h)"]
    
    with open(fnameeig, "w", newline='') as f:
         writer = csv.DictWriter(f, delimiter="&", fieldnames = header)
         writer.writeheader()
    for k in range(3):
        vlams.append([])
    for i in range(n_cut_ref + 1):
        print("refinement is: ", i)
        if i==0:
#            a,b,pre, Vh, VhG = getevproblem(mesh, phi, lset_approx, deformation, order)
            pass
        if i != 0:
            lset_approx, deformation = refineMesh(mesh, phi, order)
        
        vlam, ev, VhG, a,pre, ci,start = computeEV(phi,mesh,lset_approx, deformation, order, condense=condense)
        print("time: ", time.time()-start)
        print("condense: ", condense)
        print("eigenvalues:", vlam)
        
        with open(fnameeig, "a", newline='') as f:
            values = [i]+[lam for lam in vlam]
            dictionary = dict(zip(header, values))

            writer = csv.DictWriter(f, delimiter=",", fieldnames = header)
            writer.writerow(dictionary)
        with open(fname, "a") as f:
            f.write("eigenvalues for refinement "+ str(i)+":\n")
            f.write(str(vlam) + "\n")
#            vlam, ev = computeEV2(Vh, VhG, a, b, pre)
        for k in range(3):
            vlams[k].append(vlam[k])
            print("vlamk: ", vlam[k])
        if i < 2 and i != 0:
            symmetries = 0
            for k in range(3):
#                if math.log(abs(1 - vlams[k][1]), 10) < math.log(abs(vlams[k][0] - vlams[k][1]), 10):
                if math.log(abs( vlams[k][1]), 2) <-(order+1)*(i):
                    print("corresponding log: ")
                    print(math.log(abs( vlams[k][1]), 2))
                    
                    print("condition: ", -(order+1)*(i+1))
                    symmetries += 1
        elif i >= 2:
            symmetries = 0
            with open(fname, "a") as f:
                f.write("Condition for discrete KF: "+ str(-(args.sca*order+1) * (i))+":\n")
                for j in range(3):
                    temp = 0
                    temp += vlams[j][-1]
                    temp -= (vlams[j][-1] - vlams[j][-2]) ** 2 / (
                                (vlams[j][-1] - vlams[j][-2]) - (vlams[j][-2] - vlams[j][-3]))
    #                if math.log(abs(1 - vlams[j][-1]), 10) < math.log(abs(temp - vlams[j][-1]), 10):
                    print("corresponding log: ")
                    print(math.log(abs( temp), 2))
                    print("condition: "+ str(-(args.sca*order+1)*(i+1)))
                    print("log10: "+ str(math.log(abs( temp), 10)))
                    
                    f.write("Actual value: "+str(math.log(abs( temp), 2)) + "\n")
                    if False or math.log(abs( temp), 2) < -(args.sca*order+1) * (i):

                        symmetries += 1
                f.write("Symmetries: " + str(symmetries)+"\n")
        if True and i!=0:
            print("starting solve..")
            print("symmetries: ", symmetries)
            with TaskManager():
                l2err, l2errnokf, uerr, uh, uSolnokf = getKilling(mesh, lset_approx, deformation, vlam, ev, VhG, a, pre, ci, step=i, alpha=args.alpha, maxsym = symmetries, maxsymex = maxk, condense=condense, Ps = Ps, phi=phi)
            l2errors.append(l2err)
            l2errkf.append(l2errnokf)
    with open(fname, "a") as f:
        f.write("l2 errors: \n")
        f.write(str(l2errors)+ "\n")
        f.write("l2 errors for LaTeX: \n")
        f.write(" & ".join([str(l2) for l2 in l2errors]) + "\n")
        f.write("l2 errors without discrete KF: \n")
        f.write(str(l2errkf)+ "\n")
        f.write("l2 errors without discrete KF for LaTeX: \n")
        f.write(" & ".join([str(l2) for l2 in l2errkf])+ "\n")
        
    fnameerrs = fname + "l2errs"
    header = [str(k) for k in range(1, n_cut_ref+1)]
    with open(fnameerrs, "w", newline='') as f:
            
            dictionary = dict(zip(header, l2errors))

            writer = csv.DictWriter(f, delimiter=",", fieldnames = header)
            writer.writeheader()
            writer.writerow(dictionary)
            dictionary = dict(zip(header, l2errkf))
            writer.writerow(dictionary)
    print(l2errors)
    print("error with no killing field: ")
    print(l2errkf)

    if args.plot:
        if geom=="ellipsoid" and c==1:

            plot_errors(l2errors, l2errkf, "sphere", args.saveplot, order=order, refs=n_cut_ref, c=c, sca=args.sca)

        else:
            plot_errors(l2errors, l2errkf, "ellipsoid", args.saveplot, order=order, refs=n_cut_ref, c=c, sca=args.sca)

    if vtkout:  # and len(sys.argv) < 2 or not (sys.argv[1] == "testmode"):
        #    input("Continue (press enter) to create a VTK-Output to stokestracefem3d.vtk")

        #    with TaskManager():

        vtk = VTKOutput(ma=mesh,
                        coefs=[lset_approx, deformation, uh, uerr, uSolnokf],
                        names=["P1-levelset", "deformation", "uh", "uerr", "uex"],
                        filename=args.fname, subdivision=4, legacy=False)

        vtk.Do()
