
\documentclass{article}

\usepackage[margin=1in]{geometry} 
\usepackage{amsmath,amsthm,amssymb,hyperref, mathtools}
\usepackage[short, nodayofweek,ddmmyyyy]{datetime}

\usepackage{stix}
\DeclareMathAlphabet\mathbfcal{LS2}{stixcal}{b}{n}
\usepackage{cite}
\usepackage{cleveref}
\newcommand{\R}{\mathbb{R}}  
\newcommand{\Z}{\mathbb{Z}}
\newcommand{\N}{\mathbb{N}}
\newcommand{\Q}{\mathbb{Q}}
\newcommand{\set}[1]{\{#1\}}
\newcommand{\surfdiv}{\operatorname{div}_\Gamma}
\newcommand{\sgrad}{\nabla_\Gamma}
\newcommand{\fut}{\mathbf{u}_\tau}
\newcommand{\fvt}{\mathbf{v}_\tau}
\newcommand{\blinner}[2]{(#1,#2)_{L^2(\Gamma)}}
\newcommand{\normal}[0]{\mathbf{n}}
\newcommand{\bldinner}[2]{(#1,#2)_{L^2(\Gamma_h)}}
\newcommand{\taylorhood}[1]{\mathbfcal{P}_{#1}-\mathcal{P}_{#1-1}}
\newcommand{\dsgrad}{\nabla_{\Gamma_h}}
\newcommand{\ghint}[1]{\int_{\Gamma_h}#1 \ ds_h}
\newcommand{\dotargs}{(\cdot, \cdot)}
\DeclarePairedDelimiterX{\norm}[1]{\lVert}{\rVert}{#1}
\DeclarePairedDelimiterX{\abs}[1]{\lvert}{\rvert}{#1}
\newcommand{\ghnorm}[1]{\norm{#1}_{L^2(\Gamma_h)}}
\newcommand{\gnorm}[1]{\norm{#1}_{L^2(\Gamma)}}

\newtheorem{problem}{Problem}

\begin{document}

	\title{A trace finite element method for the surface Vector-Laplacian} 
	\maketitle
	
	\section{Continuous Problem}
	In the following, we will mostly use the notation and results given in \cite{TJ_PHD_2021}.
We consider the surface Vector-Laplacian, which for a given (sufficiently regular) surface $\Gamma\subset \R^3$ reads as follows:
{\begin{problem} 
	For a given tangential force vector field  $\mathbf{f}_\tau: \Gamma \rightarrow \R^3$, determine a tangential velocity $\mathbf{u}_\tau : \Gamma \rightarrow \R^3$ with $\fut \perp \mathcal{K}:=\ker  E$ such that 
	\begin{align*}
		-\mathbf{P} \surfdiv (E(\mathbf{u}_\tau)) +\sgrad p &= \mathbf{f}_\tau & \text{on } \Gamma
	\end{align*}
Here, $E(\fut)$ is the \emph{surface rate-of-strain tensor}, defined by 
\begin{align*}
	E(\fut) = \mathbf{P} (\nabla \fut + (\nabla \fut)^T)\mathbf{P}
\end{align*}
and $\mathbf{P}=\mathbf{I}-\mathbf{n}\mathbf{n}^T$ is the tangential projection, where $\mathbf{n}$ is the outer normal.
\end{problem}}
Defining
\begin{align*}
	a(\mathbf{u},\mathbf{v})&:= \int_\Gamma E(\mathbf{u}):E(\mathbf{v}) ds, \\
	k(u,v) &:= \eta \int_\Gamma (\mathbf{u}\cdot \normal)(\mathbf{v}\cdot \normal) ds
\end{align*}
and
\begin{align*}
	\mathbf{V}&:=\mathbf{H}^1(\Gamma),\\
	\mathbf{V}_\tau &:= \set{\mathbf{u}\in \mathbf{V}\vert \mathbf{u}\cdot \mathbf{n}=0}, \\
	\mathbf{V}_* &:= \set{\mathbf{u}\in \mathbf{L}^2(\Gamma)\vert \ \fut \in \mathbf{V}_\tau, u_N \in L^2(\Gamma)}
\end{align*}

the corresponding variational formulation can be written as
\begin{problem}\label{continuous}
	For a given force field $\mathbf{f}\in \mathbf{L}^2(\Gamma)$ with $\mathbf{f}\cdot \mathbf{n}=0$, determine $\fut \in \mathbf{V}_\tau$ with $\fut \perp \mathcal{K}$ such that
	\begin{align*}
		a(\fut, \fvt)&= \blinner{\mathbf{f}}{\fvt} & \forall \fvt \in \mathcal{K}^\perp
	\end{align*}
\end{problem}
\subsection{Symmetry-Breaking deformations}
Now, we define $\lambda_i$ as the $i$-th Eigenvalue of $\Delta_\Gamma:= \operatorname{div}_\Gamma(E(\cdot))$. We assume that $\lambda_1=\epsilon>0$ and $\lambda_i>c>\epsilon$ for all $i>1$ for a fixed $c \in \R$ and we rewrite \cref{continuous} in the following way:
\begin{problem} \label{opnotation}
	for a given r.h.s. $\mathbf{f} \in (\mathbf{L}^2(\Gamma))^*$ find a $\fut \in V_\tau$ such that 
\begin{align*}
	A(\fut) = \mathbf{f}
\end{align*}
with $A$ being the operator associated with the bilinear form $a$.
\end{problem}
This problem is well-posed for any $\epsilon$ as $\ker A = \set{0}$, for further details see \cite{TJ_PHD_2021}. The operator $A: V_\tau \rightarrow (\mathbf{L}^2(\Gamma))^*$ is bijective and has an inverse, additionally there holds that any eigenvector of $A$ corresponding to an eigenvalue $\lambda_i$ is an eigenvector of $A^{-1}$ corresponding to the eigenvalue $\lambda_i^{-1}$ of $A^{-1}$, so we can conclude the following:
For a solution $\fut \in V_\tau$ to \cref{opnotation} with a r.h.s. $\tilde{\mathbf{f}}:= \mathbf{f} + \mathbf{w}$ for $\mathbf{w}$ being an eigenvector of $A$ corresponding to the eigenvalue $\lambda_1$ there holds
\begin{align*}
	A(\fut) &= \tilde{\mathbf{f}} \\
	\Rightarrow \qquad \fut &= A^{-1}(\mathbf{f}+\mathbf{w})\\
	\Rightarrow \qquad \fut &= A^{-1}\mathbf{f}+\frac{1}{\epsilon} \mathbf{w}
\end{align*}
This implies that a perturbation of the r.h.s. aligning with the almost Killing Vector Space w.r.t. $\lambda_1$ results in a perturbation of the solution scaling with $\frac{1}{\epsilon}$.

\subsection{Time-Dependent problem and deformations}
To look at the effect of symmetry-breaking deformations on time-dependent problems we define the problem 
\begin{problem}\label{timeprob}
	For a given tangential force vector field $\mathbf{f}_\tau: \Gamma \times [0,T) \rightarrow \R^3$ and an initial velocity $\mathbf{u}_0:\Gamma \rightarrow \R^3$ with $\mathbf{u}_0 \cdot \mathbf{n} = 0$, determine a tangential velocity field $\fut : \Gamma \times  [0,T) \rightarrow \R^3$ such that 
	\begin{align*}
		\partial_t \fut - \Delta_\Gamma \fut &= \mathbf{f}_\tau, \\
		\fut(\cdot, 0) &= \mathbf{u}_0(\cdot)
	\end{align*}
\end{problem}
For a solution to \cref{timeprob}, we can calculate the following:
\begin{align*}
	\partial_t \gnorm{\fut(\cdot, t)}^2 &= 2\int_\Gamma \fut(x,t) \partial_t \fut(x,t) \ d\Gamma \\
	&= 2\int_T (\Delta_\Gamma \fut(x,t) + \mathbf{f}(x,t)) \fut(x,t) \ d\Gamma \\
\end{align*}
So we can estimate
\begin{align*}
	\frac{1}{2} \partial_t \gnorm{\fut(\cdot, t)}^2 &= -\int_\Gamma E(\fut):E(\fut) \ d\Gamma + \int_\Gamma \mathbf{f}(x,t) \fut(x,t)\ d\Gamma \\
	&\leq \int_\Gamma \mathbf{f}(x,t) \fut(x,t)\ d\Gamma \leq \gnorm{\mathbf{f}(\cdot, t)} \gnorm{\fut(\cdot, t)}
\end{align*}
Dividing with $\gnorm{\fut(\cdot, t)}$ and integrating over $[0,T]$ yields
\begin{align*}
	\gnorm{\fut(\cdot, T)} \leq \int_0^T \gnorm{\mathbf{f}(\cdot, s)}\ ds + \gnorm{\fut(\cdot, 0)}
\end{align*}
So our solution is always bound by the initial data and the force vector field and as such, in contrast to the stationary case, is not sensitive to symmetry-breaking deformations.
\section{Discrete formulation}
To approximate \cref{continuous}, we consider the higher order parametric trace finite element spaces using a consistent penalty approach. For details regarding the finite element spaces, see \cite[Section 3.2.1]{TJ_PHD_2021}.

Defining the discrete rate-of-strain tensor
\begin{align*}
	E_{\tau, h}(\mathbf{u}) := \frac{1}{2}(\nabla_{\Gamma_h}\mathbf{u}+(\dsgrad \mathbf{u})^T)-(\mathbf{u}\cdot \mathbf{n}_h)\mathbf{H}_h,
\end{align*}
with $\Gamma_h$ being the discrete approximation of the surface $\Gamma$, $\dsgrad:= \mathbf{P}_h \nabla $, $\mathbf{P}_h$ the tangential projection on $\Gamma_h$, $\mathbf{n}_h$ the outer normal on $\Gamma_h$ and $\mathbf{H}_h:=\nabla(\Pi_{\Theta}^{k-1}(\mathbf{n_h}))$ an approximation to the Weingarten map allows us to define
\begin{align*}
	a_{\tau,h}(\mathbf{u},\mathbf{v})&:=\ghint{E_{\tau,h}(\mathbf{u}):E_{\tau,h}(\mathbf{v})}%+ \epsilon \ghint{\mathbf{P}_h \mathbf{u}\cdot \mathbf{P}_h\mathbf{v}},\\
\end{align*}
Further, as our function space is defined on the volume but we are only interested in tangential solutions, we define the penalty form 
\begin{align*}
	k_h(\mathbf{u},\mathbf{v}):= \eta \ghint{(\mathbf{u}\cdot \tilde{\mathbf{n}})(\mathbf{v}\cdot \tilde{\mathbf{n}})}
\end{align*}
and to achieve stability, we use normal gradient volume stabilization for both the velocity and pressure variables:
\begin{align*}
	s_h(\mathbf{u},\mathbf{v})&:=\rho_u \int_{\Omega^\Gamma_{\Theta}}(\nabla \mathbf{u}\mathbf{n}_h)\cdot (\nabla \mathbf{u}\mathbf{n}_h) dx
	%\tilde{s}_h(p,q)&:=\rho_p \int_{\Omega^\Gamma_{\Theta}}(\mathbf{n}_h\cdot \nabla p)(\mathbf{n}_h\cdot \nabla q) dx.
\end{align*}
Now, we can state the discrete surface Stokes problem:
\begin{problem}
	Find $\mathbf{u}_h\in \mathbf{U}_h $ such that 
	\begin{align*}
		A_{\tau,h}(\mathbf{u}_h.\mathbf{v}_h)&= \bldinner{\mathbf{f}_h}{\mathbf{v}_h}&\forall \mathbf{v}_h \in \mathbf{U}_h
	\end{align*}
	with $A_{\tau,h}\dotargs :=a_{\tau,h}\dotargs + s_h\dotargs + k_h\dotargs$
\end{problem}
\section{Numerical experiments}
In this section we will present results of numerical experiments. For now, we only look at the surface vector laplacian, i.e. the presented equation ommitting all bilinear and linear forms related to the pressure.

We consider the surface vector laplace equation on surfaces $\Gamma_c \subset \Omega:=[-2,2]^3$ described by the level set function 
\begin{align*}
	\phi(\mathbf{x})&:=\sqrt{x^2+y^2+\left(\frac{z}{c}\right)^2}-1, &\mathbf{x} \in \R^3.
\end{align*}
Obviously, for $c=1$ this describes the unit sphere and for $c>1$ we get ellipsoids. Additionally, with $\mathbf{P}_{\mathcal{K}^\perp}$ being the projeciton on the orthogonal complement of $\mathcal{K}$, we prescribe the solution 
\begin{align*}
	\mathbf{u}(\mathbf{x})&:=\mathbf{P}_{\mathcal{K}^\perp}\left(\mathbf{P}\left(-z^2,x,y\right)^T\right)
\end{align*}
The Killing vector fields on this geometry with $c=1$ are defined by the $L_2$-orthogonal (but not normalized) basis
\begin{align*}
	\mathbf{k}_1 &:= (y,-x,0)^T,\\
	\mathbf{k}_2 &:= (z,0,-x)^T,\\
	\mathbf{k}_3 &:= (0,z,-y)^T 
\end{align*}
and for $c>1$, only $\mathbf{k}_1$ is a Killing vector field.

For our discrete formulation, we fix the parameters
\begin{align*}
	\rho_u &= \frac{1}{h},\\
	%\rho_p &= h,\\
	\eta &= \frac{1}{h^2},%,\\
%	\epsilon &= \frac{1}{h^\alpha}
\end{align*}
where h is the corresponding mesh size, $\alpha>0$ and we use the polynomial degree $k$, i.e. a $\mathbfcal{P}_k-\mathcal{P}_{k-1}$ Taylor-Hood discretization.

Regarding the space $\mathcal{K}_h$, we say that a vector field $\mathbf{k}_h$ is a discrete killing vector field if it is a eigenvector for the discrete surface vector laplace operator with an eigenvalue $\lambda_h$ such that 
\begin{itemize}
	\item $\abs{\lambda_h} \leq h^{k+1}$ if we do not have enough Eigenvalues in our sequence $(\lambda_h)_h$ to use the Aitken delta-squared process
 \item $\abs{T\lambda_h} \leq h^{2k}$ else.
\end{itemize}
\subsection{Results on the Unit Sphere}
Defining $e^{\mathcal{K}^\perp_h}_{L^2}:= \ghnorm{\mathbf{P}_{\mathcal{K}_h^\perp}(\mathbf{u}-\mathbf{u}_h)}$ and $e_{L^2}:= \ghnorm{(\mathbf{u}-\mathbf{P}_{\mathcal{K}_h^\perp}\mathbf{u}_h)}$, we observe the following results on the unit sphere for the polynomial degrees $k \in \{2,3,4\}$:
\subsubsection{$k=2$}
\begin{table}[h]
	\begin{tabular}{|l|l|l|l|l|l|}
		\hline
		Refinement                  & 1         & 2         & 3         & 4         & 5         \\ \hline
		$e_{L^2}$ & 4.4468e-2 & 7.3299e-3 & 8.7247e-4 & 1.0255e-4 & 1.2524e-5 \\ \hline
		$e^{\mathcal{K}^\perp_h}_{L^2}$                          & 4.8267e-2 & 7.4900e-3 & 8.3629e-4 & 9.4380e-5 & 1.1275e-5 \\ \hline
		\end{tabular}
	\caption{Convergence results on the sphere with $k=2$. The convergence order is $h^3$ as we are able to correctly identify $\mathcal{K}_h$}
	\label{tab:sphe3}
	\end{table}
	\begin{table}[h]
		\begin{tabular}{|l|l|l|l|l|l|l|}
		\hline
		Refinement                   & 0         & 1         & 2         & 3         & 4         & 5         \\ \hline
		$\lambda_1(h)$ & 1.6837e-2 & 1.2195e-3 & 7.5317e-5 & 4.3124e-6 & 2.5909e-7 & 1.6005e-8 \\ \hline
		$\lambda_2(h)$ & 2.3342e-2 & 1.5471e-3 & 8.0450e-5 & 4.8016e-6 & 2.8988e-7 & 1.7813e-8 \\ \hline
		$\lambda_3(h)$ & 2.8397e-2 & 1.8813e-3 & 8.4883e-5 & 4.9095e-6 & 2.9726e-7 & 1.8241e-8 \\ \hline
		$\lambda_4(h)$ & 9.8074e-1 & 9.9874e-1 & 9.9981e-1 & 1.0000e+0 & 1.0000e+0 & 1.0000e+0 \\ \hline
		\end{tabular}
		\caption{The first 4 discrete Eigenvalues for $k=2$.}
		\label{tab:my-table}
		\end{table}
		\subsubsection{$k=3$}
		\begin{table}[h!]
			\begin{tabular}{|l|l|l|l|l|l|}
			\hline
			Refinements & 1         & 2         & 3         & 4         & 5         \\ \hline
			$e_{L^2}$			& 3.2815e-2 & 3.6020e-3 & 3.0284e-4 & 1.8892e-5 & 1.2444e-6 \\ \hline
			$e^{\mathcal{K}^\perp_h}_{L^2}$ 			& 3.5884e-2 & 3.8927e-3 & 3.1455e-4 & 1.9677e-5 & 1.2900e-6 \\ \hline
			\end{tabular}
			\caption{Convergence results on the sphere with $k=3$. The convergence order is $h^4$ as we are able to correctly identify $\mathcal{K}_h$}
			\label{tab:my-table}
			\end{table}
			\begin{table}[h!]
				\begin{tabular}{|l|l|l|l|l|l|l|}
				\hline
				Refinement   & 0         & 1         & 2         & 3         & 4          & 5          \\ \hline
				$\lambda_1(h)$ & 4.2380e-3 & 8.0803e-5 & 2.4902e-6 & 3.7379e-8 & 5.5588e-10 & 8.6275e-12 \\ \hline
				$\lambda_2(h)$ & 4.8303e-3 & 9.9281e-5 & 3.0997e-6 & 4.1370e-8 & 5.9021e-10 & 9.0313e-12 \\ \hline
				$\lambda_3(h)$ & 6.4686e-3 & 1.0859e-4 & 3.9798e-6 & 4.7057e-8 & 7.1631e-10 & 1.0381e-11 \\ \hline
				$\lambda_4(h)$ & 9.5310e-1 & 9.9834e-1 & 9.9983e-1 & 9.9999e-1 & 1.0000e+0  & 1.0000e+0  \\ \hline
				\end{tabular}
				\caption{The first 4 discrete Eigenvalues for $k=3$.}
				\label{tab:my-table}
				\end{table}
				\newpage
			\subsubsection{$k=4$}
			\begin{table}[h!]
				\begin{tabular}{|l|l|l|l|l|}
					\hline
					Refinements                     & 1         & 2         & 3         & 4         \\ \hline
					$e_{L^2}$                       & 9.2043e-3 & 5.1825e-4 & 2.1655e-5 & 5.9460e-7 \\ \hline
					$e^{\mathcal{K}^\perp_h}_{L^2}$ & 9.3262e-3 & 5.2730e-4 & 2.2159e-5 & 6.0913e-7 \\ \hline
					\end{tabular}
				\caption{Convergence results on the sphere with $k=4$. The convergence order is $h^5$ as we are able to correctly identify $\mathcal{K}_h$}
				\label{tab:my-table}
				\end{table}
				\begin{table}[h!]
					\begin{tabular}{|l|l|l|l|l|l|}
					\hline
					Refinement   & 0         & 1         & 2         & 3          & 4          \\ \hline
					$\lambda_1(h)$ & 4.2213e-3 & 6.4017e-6 & 2.5756e-8 & 9.1384e-11 & 3.1026e-13 \\ \hline
					$\lambda_2(h)$ & 4.4598e-3 & 6.5209e-6 & 2.6908e-8 & 1.0540e-10 & 3.2579e-13 \\ \hline
					$\lambda_3(h)$ & 5.6937e-3 & 1.1350e-5 & 3.4116e-8 & 1.2119e-10 & 4.8317e-13 \\ \hline
					$\lambda_4(h)$ & 1.1693e+0 & 9.9993e-1 & 1.0043e+0 & 1.0000e+0  & 1.0000e+0  \\ \hline
					\end{tabular}
					\caption{The first 4 discrete Eigenvalues for $k=4$.}
					\label{tab:my-table}
					\end{table}
			
\subsection{Result on an Ellipsoid with $c=1.1$}
We fix $c=1.1$ as to have $\lambda_1=0$ and $\lambda_2 \neq 0 \neq \lambda_3$. In this case, $\lambda_2$ and $\lambda_3$ have enough distance to $0$ that all polynomial degrees are able to correctly identify the dimension of $\mathcal{K}_h$, as we see in the following tables:
\subsubsection{$k=2$}
\begin{table}[h!]
\begin{tabular}{|l|l|l|l|l|l|}
\hline
Refinements                     & 1         & 2         & 3         & 4         & 5         \\ \hline
$e_{L^2}$                       & 1.5410e+0 & 3.3307e-2 & 5.8750e-3 & 2.4897e-4 & 2.2246e-5 \\ \hline
$e^{\mathcal{K}^\perp_h}_{L^2}$ & 4.9353e-2 & 3.3423e-2 & 5.8754e-3 & 2.4682e-4 & 2.1703e-5 \\ \hline
\end{tabular}
\caption{Errors on the ellipsoid with $c=1.1$. after enough refinements, we are able to correctly identify the Killing vector fields.}
\label{tab:my-table}
\end{table}
\begin{table}[h!]
\begin{tabular}{|l|l|l|l|l|l|l|}
\hline
Refinement   & 0         & 1         & 2         & 3         & 4         & 5         \\ \hline
$\lambda_1(h)$ & 2.2297e-2 & 9.6303e-4 & 7.5937e-5 & 4.2562e-6 & 2.6360e-7 & 1.6484e-8 \\ \hline
$\lambda_2(h)$ & 2.6898e-2 & 3.8147e-3 & 2.9701e-3 & 2.9029e-3 & 2.8990e-3 & 2.8987e-3 \\ \hline
$\lambda_3(h)$ & 3.2410e-2 & 3.9503e-3 & 2.9817e-3 & 2.9033e-3 & 2.8990e-3 & 2.8987e-3 \\ \hline
$\lambda_4(h)$ & 8.8028e-1 & 9.0682e-1 & 8.9471e-1 & 8.9442e-1 & 8.9270e-1 & 8.9212e-1 \\ \hline
\end{tabular}
\caption{Discrete Eigenvalues for the ellipsoid with $c=1.1$.}
\label{tab:my-table}
\end{table}
\newpage
\subsubsection{$k=3$}
\begin{table}[h]
\begin{tabular}{|l|l|l|l|l|l|}
\hline
Refinements & 1         & 2         & 3         & 4         & 5         \\ \hline
$e_{L^2}$			& 1.5382e+0 & 1.2025e-2 & 1.3409e-3 & 8.8007e-5 & 4.9894e-6 \\ \hline
$e^{\mathcal{K}^\perp_h}_{L^2}$			& 3.4647e-2 & 1.2116e-2 & 1.3428e-3 & 8.8150e-5 & 4.9997e-6 \\ \hline
\end{tabular}
\caption{Errors on the ellipsoid with $c=1.1$. after enough refinements, we are able to correctly identify the Killing vector fields.}
\label{tab:my-table}
\end{table}

\begin{table}[h]
\begin{tabular}{|l|l|l|l|l|l|l|}
\hline
Refinement   & 0         & 1         & 2         & 3         & 4          & 5          \\ \hline
$\lambda_1(h)$ & 6.4664e-3 & 5.5865e-5 & 1.7950e-6 & 2.5822e-8 & 4.1884e-10 & 6.3698e-12 \\ \hline
$\lambda_2(h)$ & 9.6209e-3 & 2.9739e-3 & 2.9014e-3 & 2.8988e-3 & 2.8987e-3  & 2.8987e-3  \\ \hline
$\lambda_3(h)$ & 1.1687e-2 & 3.0021e-3 & 2.9028e-3 & 2.8989e-3 & 2.8987e-3  & 2.8987e-3  \\ \hline
$\lambda_4(h)$ & 8.5185e-1 & 8.9105e-1 & 8.9064e-1 & 8.9624e-1 & 8.9399e-1  & 8.9115e-1  \\ \hline
\end{tabular}
\caption{Discrete Eigenvalues for the ellipsoid with $c=1.1$.}
\label{tab:my-table}
\end{table}
\subsubsection{$k=4$}
\begin{table}[h]
\begin{tabular}{|l|l|l|l|l|}
\hline
Refinements & 1         & 2         & 3         & 4         \\ \hline
$e_{L^2}$				& 1.5370e+0 & 3.8283e-3 & 2.6980e-5 & 5.7360e-7 \\ \hline
$e^{\mathcal{K}^\perp_h}_{L^2}$				& 9.8160e-3 & 3.8335e-3 & 2.7129e-5 & 5.8409e-7 \\ \hline
\end{tabular}
\caption{Errors on the ellipsoid with $c=1.1$. after enough refinements, we are able to correctly identify the Killing vector fields.}
\label{tab:my-table}
\end{table}
\begin{table}[h!]
\begin{tabular}{|l|l|l|l|l|l|}
\hline
Refinement   & 0         & 1         & 2         & 3          & 4          \\ \hline
$\lambda_1(h)$ & 4.1782e-3 & 2.6238e-6 & 3.0175e-8 & 7.7597e-11 & 3.1514e-13 \\ \hline
$\lambda_2(h)$ & 6.6929e-3 & 2.9010e-3 & 2.8989e-3 & 2.8987e-3  & 2.8987e-3  \\ \hline
$\lambda_3(h)$ & 7.0178e-3 & 2.9041e-3 & 2.9183e-3 & 2.8990e-3  & 2.8988e-3  \\ \hline
$\lambda_4(h)$ & 1.0015e+0 & 9.0622e-1 & 8.9792e-1 & 9.1123e-1  & 8.9351e-1  \\ \hline
\end{tabular}
\caption{Discrete Eigenvalues for the ellipsoid with $c=1.1$.}
\label{tab:my-table}
\end{table}
\subsection{Result on an Ellipsoid with $c=1.001$}
On an ellipsoid with $c=1.001$, the situation is more difficult: While we are able to find $\dim \mathcal{K}_h$ after enough refinements, even more refinements are needed for convergence, as we still have a significant error in the almost-killing modes in the preasymptotic regime. In other words, we are able to infer $\dim \mathcal{K}_h$ but just removing these modes does not yield satisfying results in the preasymptotic range.
\subsubsection{$k=2$}
\begin{table}[h!]
\begin{tabular}{|l|l|l|l|l|l|}
\hline
Refinements & 1         & 2         & 3         & 4         & 5         \\ \hline
$e_{L^2}$			& 1.4524e+0 & 1.4485e+0 & 1.4481e+0 & 1.4481e+0 & 1.2135e-1 \\ \hline
$e^{\mathcal{K}^\perp_h}_{L^2}$				& 4.8258e-2 & 7.4460e-3 & 8.3566e-4 & 9.4373e-5 & 1.2134e-1 \\ \hline
\end{tabular}
\caption{Errors on the ellipsoid with $c=1.001$ and polynomial degree $k=2$. We need significant refinement to be able to identify $\dim \mathcal{K}_h$. Other experiments suggest that even more refinements would yield convergence of order $h^3$, but this might be prohibitively expensive in practice.}
\label{tab:my-table}
\end{table}
\begin{table}[h!]
	\begin{tabular}{|l|l|l|l|l|l|l|}
	\hline
	Refinement   & 0         & 1         & 2         & 3         & 4         & 5         \\ \hline
	$\lambda_1(h)$ & 1.6851e-2 & 1.2196e-3 & 7.5720e-5 & 4.6148e-6 & 2.9639e-7 & 1.8201e-8 \\ \hline
	$\lambda_2(h)$ & 2.3363e-2 & 1.5475e-3 & 8.0766e-5 & 4.8940e-6 & 5.7898e-7 & 3.3569e-7 \\ \hline
	$\lambda_3(h)$ & 2.8389e-2 & 1.8806e-3 & 8.5043e-5 & 5.1433e-6 & 6.0906e-7 & 3.3748e-7 \\ \hline
	$\lambda_4(h)$ & 9.7996e-1 & 9.9806e-1 & 9.9916e-1 & 9.9934e-1 & 9.9935e-1 & 9.9933e-1 \\ \hline
	\end{tabular}
	\caption{The first 4 discrete eigenvalues of the surface vector-laplacian. Significant refinements are needed to be able to distinguish $\lambda_1(h)$ from $\lambda_2(h)$ and $\lambda_3(h)$.}
	\label{tab:my-table}
\end{table}
\newpage
\subsubsection{$k=3$}
\begin{table}[h!]
\begin{tabular}{|l|l|l|l|l|l|}
\hline
Refinements & 1         & 2         & 3         & 4         & 5         \\ \hline
$e_{L^2}$			& 1.4494e+0 & 1.4483e+0 & 1.4481e+0 & 7.0436e-1 & 4.4310e-2 \\ \hline
$e^{\mathcal{K}^\perp_h}_{L^2}$			
			& 3.5903e-2 & 3.8951e-3 & 3.1447e-4 & 7.0436e-1 & 4.4310e-2 \\ \hline
\end{tabular}
\caption{Errors on the ellipsoid with $c=1.001$ and polynomial degree $k=3$. We need significant refinement to be able to identify $\dim \mathcal{K}_h$.}
\label{tab:my-table}
\end{table}
\begin{table}[h!]
	\begin{tabular}{|l|l|l|l|l|l|l|}
	\hline
	Refinement   & 0         & 1         & 2         & 3         & 4          & 5          \\ \hline
	$\lambda_1(h)$ & 4.2483e-3 & 8.1473e-5 & 2.6269e-6 & 4.2310e-8 & 6.0766e-10 & 9.3793e-12 \\ \hline
	$\lambda_2(h)$ & 4.8354e-3 & 9.9297e-5 & 3.2792e-6 & 3.5746e-7 & 3.2027e-7  & 3.1969e-7  \\ \hline
	$\lambda_3(h)$ & 6.4767e-3 & 1.0901e-4 & 4.3575e-6 & 3.6570e-7 & 3.2040e-7  & 3.1969e-7  \\ \hline
	$\lambda_4(h)$ & 9.5260e-1 & 9.9791e-1 & 9.9920e-1 & 9.9935e-1 & 9.9933e-1  & 9.9932e-1  \\ \hline
	\end{tabular}
	\caption{The first 4 discrete eigenvalues of the surface vector-laplacian. Significant refinements are needed to be able to distinguish $\lambda_1(h)$ from $\lambda_2(h)$ and $\lambda_3(h)$.}
	\label{tab:my-table}
	\end{table}
\subsubsection{$k=4$}
\begin{table}[h!]
	\begin{tabular}{|l|l|l|l|l|}
	\hline
	Refinements & 1         & 2         & 3         & 4         \\ \hline
	$e_{L^2}$			& 1.4481e+0 & 1.4481e+0 & 2.4034e-1 & 7.0095e-4 \\ \hline
	$e^{\mathcal{K}^\perp_h}_{L^2}$				& 9.3398e-3 & 5.2706e-4 & 2.4034e-1 & 7.0095e-4 \\ \hline
	\end{tabular}
	\caption{Errors on the ellipsoid with $c=1.001$ and polynomial degree $k=3$. After 3 refinements, we are able to correctly identify the discrete killing vector fields and get to asymptotic behaviour.}
	\label{tab:my-table}
	\end{table}
	\begin{table}[h!]
		\begin{tabular}{|l|l|l|l|l|l|}
		\hline
		Refinement   & 0         & 1         & 2         & 3          & 4          \\ \hline
		$\lambda_1(h)$ & 4.2112e-3 & 6.4720e-6 & 2.9327e-8 & 1.0468e-10 & 3.8841e-13 \\ \hline
		$\lambda_2(h)$ & 4.4464e-3 & 6.8384e-6 & 3.4611e-7 & 3.1976e-7  & 3.1968e-7  \\ \hline
		$\lambda_3(h)$ & 5.6704e-3 & 1.1745e-5 & 3.5161e-7 & 3.1979e-7  & 3.1968e-7  \\ \hline
		$\lambda_4(h)$ & 1.1681e+0 & 9.9928e-1 & 1.0042e+0 & 9.9932e-1  & 9.9932e-1  \\ \hline
		\end{tabular}
		\caption{The first 4 discrete eigenvalues of the surface vector-laplacian.}
		\label{tab:my-table}
		\end{table}
\newpage
\section{Summary}
Our numerical experiments suggest that we are able to correctly identify the dimension of $\mathcal{K}$, even if the corresponding eigenvalues are small, if our polynomial degree is large enough or we refine our mesh sufficiently. In the preasymptotic range however, this does not translate to convergent behaviour of $e_{L^2}$, this requires more refinements or a higher polynomial degree compared to the first result.


	\bibliographystyle{plain}
\bibliography{references}

\end{document}